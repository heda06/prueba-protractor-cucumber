import {ElementFinder, element, by, ElementArrayFinder} from "protractor"

/** Clase que contienen los elemntos de la pagina de login de Banca March. */
export class HomePage
{
    /** Elemento para el boton para elegir la ciudad. */
    selectorCiudad:ElementFinder;
    /** Lista de elementos ciudad. */
    listCity:ElementArrayFinder
    /** Lista de elementos de botones del tipo de producto. */
    listType:ElementArrayFinder;
    /** Lista de elementos de botones de las marcas. */
    listBrand:ElementArrayFinder;
    /** Lista elementos de modelos. */
    listModel:ElementArrayFinder;
     /**Elemento de detalles de modelo. */
    detailModel:ElementFinder;
   
     /**
   * Inicializa los elementos para el login
   */
    constructor()
    {
       this.selectorCiudad= element(by.className('nav-link-label flex-row align-center font-body-2 tc-secondary uppercase font-medium'));
       this.listCity = element.all(by.className("city"));
       this.listType = element.all(by.className('flex-1 flex-row-center flheight product-line ng-star-inserted'));
       this.listBrand = element.all(by.className('grid-item text-center flex-row-center web-view brand-dis ng-star-inserted'));
       this.listModel = element.all(by.className('grid-item text-center flex-row-center web-view product-dis pad8 ng-star-inserted'));
       this.detailModel = element(by.className('pd-detail flex-row flex-col-mob ng-star-inserted'));
    }

}