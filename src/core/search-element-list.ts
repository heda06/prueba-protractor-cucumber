import { ElementFinder, element, by, ElementArrayFinder } from "protractor"
import { async } from "q";
import chai = require('chai');
var expects = chai.expect;

export class SearchElementList {
  /**
  * Localiza un elemento en una lista de elementos mediante comparacion de texto
  *  @param {ElementArrayFinder} listElement Lista de elementos
  *  @param {string} flag Texto que se va utlizar para  localizar el elemento de la lista .
  */
  public async findElementAndClick(listElement: ElementArrayFinder, flag: string) {
    await listElement.each(async (element) => {
      element.getText().then(async function (flag1) {
        if (flag1 == flag) {
          expects(flag1).to.equal(flag);
          await element.click();
        }
      })
    })
  }

/**
  * Localiza un elemento en una lista de elementos mediante comparacion de atributo
  *  @param {ElementArrayFinder} listElement Lista de elementos
  *  @param {string} atribute Tipo de atributo por el que se va a buscar.
  *  @param {string} flag Texto que se va utlizar para  localizar el elemento de la lista .
  *  @param {string} text Texto que se va a escribir.
  */
 public async findElementByAttributeAndSendKeys(listElement: ElementArrayFinder,atribute: string, flag: string, text: string ) {
  await listElement.each(async (element) => {
    element.getAttribute(atribute).then(async function (flag1) {
      if (flag1 == flag) {
        expects(flag1).to.equal(flag);
        await element.sendKeys(text);
      }
    })
  })
}

  /**
  * Localiza un elemento en una lista de elementos mediante comparacion de texto
  *  @param {ElementArrayFinder} listElement Lista de elementos
  *  @param {string} flag Texto que se va utlizar para  localizar el elemento de la lista .
  *  @param {string} text Texto que se va a escribir.
  */
 public async findElementAndSendKeys(listElement: ElementArrayFinder, flag: string, text: string) {
  await listElement.each(async (element) => {
    element.getText().then(async function (flag1) {
      if (flag1 == flag) {
        expects(flag1).to.equal(flag);
        await element.sendKeys(text);
      }
    })
  })
}

/**
* Localiza un elemento en una lista de elementos mediante comparacion de texto
*  @param {ElementArrayFinder} listElement Lista de elementos
*  @param {string} atribute Tipo de atributo por el que se va a buscar.
*  @param {string} flag Texto que se va utlizar para  localizar el elemento de la lista .
*/
public async findElementByAttributeAndClick(listElement: ElementArrayFinder,atribute: string, flag: string) {
await listElement.each(async (element) => {
  element.getAttribute(atribute).then(async function (flag1) {
    if (flag1 == flag) {
      expects(flag1).to.equal(flag);
      await element.click();
    }
  })
})
}

  /**
    * Localiza un subelemento en una lista de elementos mediante comparacion de atributo
    *  @param {ElementArrayFinder} listElement  Lista de elementos.
    *  @param {string} father Elemento padre.
    *  @param {string} child Elemento hijo.
    *  @param {string} atribute Tipo de atributo por el que se va a buscar.
    *  @param {string} flag Texto que se va utlizar para  localizar el elemento de la lista .
    */
  public async findSubElementByAttributeAndClick(listElement: ElementArrayFinder, father: string, child: string, atribute: string, flag: string) {
    await listElement.each(async (item) => {
      await item.element(by.className(father)).$(child).getAttribute(atribute).then(async (flag1) => {
        if (flag1 == flag) {
          expects(flag1).to.equal(flag);
          await item.click().then(async () => {
          })
        }
      })
    })
  }

  /**
    * Localiza un subelemento en una lista de elementos mediante comparacion de texto
    *  @param {ElementArrayFinder} listElement  Lista de elementos.
    *  @param {string} father Elemento padre.
    *  @param {string} child Elemento hijo.
    *  @param {string} flag Texto que se va utlizar para  localizar el elemento de la lista .
    */
   public async findSubElementAndClick(listElement: ElementArrayFinder, father: string, child: string,flag: string) {
    await listElement.each(async (item) => {
      await item.element(by.className(father)).$(child).getText().then(async (flag1) => {
        if (flag1 == flag) {
          expects(flag1).to.equal(flag);
          await item.click().then(async () => {
          })
        }
      })
    })
  }

}