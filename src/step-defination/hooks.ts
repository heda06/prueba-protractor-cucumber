import {After, Before, Status} from "cucumber"
import { browser } from "protractor";

After(async function(scenario) {
  
    console.log("Test terminado")
      const screenshotFail = await browser.takeScreenshot();
      this.attach(screenshotFail,"image/png");
  
  });