import { Given, When, Then } from "cucumber";
import { browser, by } from "protractor";
import chai = require('chai');
import { async } from "q";

import { AsyncResource } from "async_hooks";

import { WebElementCondition } from "selenium-webdriver";
import { HomePage } from "../page-objets/home-page";
import { SearchElementList } from "../core/search-element-list";
//import { log4jsconfig } from "../log-config/log4jsconfig"

/** Variable global de verifcacion*/
var { setDefaultTimeout } = require('cucumber');
setDefaultTimeout(60 * 1000);
var expects = chai.expect;
var log4js = require('log4js');
log4js.configure('./log-config/log4js.json');
let home = new HomePage();
let core = new SearchElementList();
var log = log4js.getLogger("startup");


Given('I will navigate to {string}', async function (url) {
  await browser.get(url);
  await browser.waitForAngularEnabled(false);

});



When('I am searching for the city {string}', async function (string) {
 
  await browser.sleep(2000);

  if (!await home.selectorCiudad.isDisplayed()) {
    
    await home.selectorCiudad.click();
  }
  core.findElementAndClick(home.listCity,string);
  // await home.listCity.each(async (element) => {
  //   element.getText().then(async function (text) {
  //     if (text == string) {
  //       await element.click()
  //     }
  //   })
  // })
  await browser.sleep(2000);
});


When('I click on  button with the image of {string} {string}', async (string, string2) => {
  // Write code here that turns the phrase above into concrete actions

  if (string == "type") {

    core.findElementAndClick(home.listType,string2)
  }
  else if (string == "brand")
  {
    core.findSubElementByAttributeAndClick(home.listBrand,'flex-col-center flwidth flheight cursor item pad16 ng-star-inserted','img.img-res','alt',string2);
    // await home.listBrand.each(async (item) =>{
    //     await item.element(by.className('flex-col-center flwidth flheight cursor item pad16 ng-star-inserted')).$('img.img-res').getAttribute('alt').then(async (text) => {
    //       if (text == string2) {
    //       expects(text).to.equal(string2);
    //       await item.click().then(async () => {
       
    //       })
    //    }
    //   })
     
    
    // })

  }

     

    
  await browser.sleep(2000)
});


When('I click on the button of the product {string}', async (string)=> {
  core.findSubElementByAttributeAndClick(home.listModel,'flex-col-center flwidth flheight cursor item ng-star-inserted','img.img-res','alt',string);
//   await home.listModel.each(async (item) =>{
    
//     await item.element(by.className('flex-col-center flwidth flheight cursor item ng-star-inserted')).$('img.img-res').getAttribute('alt').then(async (text) => {
  
//       if (text == string) {
//       expects(text).to.equal(string);
//       await item.click().then(async () => {
        
//       })
//    }
//   })
     
//   

// })
await browser.sleep(2000)
});


Then('The price is {string}', async (string)=> {
  // Write code here that turns the phrase above into concrete actions
  await home.detailModel.$('meta:nth-child(3)').getAttribute('content').then(async (text) => {
      expects(text).to.equal(string);
      console.log("Precio correcto")
     
   
  })
});