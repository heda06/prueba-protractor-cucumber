import {Config} from "protractor"
import * as reporter from "cucumber-html-reporter"
export let config: Config = {

    directConnect:false,
    framework:'custom',
    frameworkPath: require.resolve('protractor-cucumber-framework'),
    useBlockingProxy: true,
    webDriverLogDir: './logs',
    
    capabilities: {
        browserName: 'chrome',
        chromeOptions: {
            args: [
              '--disable-gpu', 
              'start-maximized'
              //,'--window-size=800,600'
            ]
          }
    },
    
    specs: ['../features/busqueda.feature'],
    cucumberOpts: {
       
        format:'json:./report/cucumber-report.json',
      
      //  tags:['@calculatorTesting'],
        require: [
            './src/step-defination/*.js'
        ]
    },
    onComplete: () =>{

        var options = {
        theme: 'bootstrap',
        jsonFile: './report/cucumber-report.json',
        output: './report/cucumber-report.html',
        reportSuiteAsScenarios: true,
        launchReport: true,
        metadata: {
            "App Version":"0.3.2",
            "Test Environment": "STAGING",
            "Browser": "Chrome  54.0.2840.98",
            "Platform": "Windows 10",
            "Parallel": "Scenarios",
            "Executed": "Remote"
    }
};

reporter.generate(options);

    }
};