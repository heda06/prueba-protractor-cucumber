Feature: Busqueda producto

Scenario: Busqueda de una tablet Asus Fonepad 7 FE375CL

Given I will navigate to "https://www.cashify.in/"
When I am searching for the city "Pune"
And I click on  button with the image of "type" "TABLET"
And I click on  button with the image of "brand" "Asus"
And I click on the button of the product "Asus Fonepad 7 FE375CL"
Then  The price is "1900"

