"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const protractor_1 = require("protractor");
const chai = require("chai");
var expects = chai.expect;
class ProtractorFunnction {
    scrollListOneLevelClick(listElement, text) {
        return __awaiter(this, void 0, void 0, function* () {
            yield listElement.each((element) => __awaiter(this, void 0, void 0, function* () {
                element.getText().then(function (flag) {
                    return __awaiter(this, void 0, void 0, function* () {
                        if (flag == text) {
                            expects(flag).to.equal(text);
                            yield element.click();
                        }
                    });
                });
            }));
        });
    }
    scrollListTwoLevelWithAtributeClick(listElement, father, child, atribute, text) {
        return __awaiter(this, void 0, void 0, function* () {
            yield listElement.each((item) => __awaiter(this, void 0, void 0, function* () {
                yield item.element(protractor_1.by.className(father)).$(child).getAttribute(atribute).then((flag) => __awaiter(this, void 0, void 0, function* () {
                    if (flag == text) {
                        expects(flag).to.equal(text);
                        yield item.click().then(() => __awaiter(this, void 0, void 0, function* () {
                        }));
                    }
                }));
            }));
        });
    }
}
exports.ProtractorFunnction = ProtractorFunnction;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvdHJhY3Rvci1mdW5jdGlvbi5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9jb3JlL3Byb3RyYWN0b3ItZnVuY3Rpb24udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLDJDQUF5RTtBQUV6RSw2QkFBOEI7QUFDOUIsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztBQUUxQixNQUFhLG1CQUFtQjtJQUVmLHVCQUF1QixDQUFDLFdBQThCLEVBQUMsSUFBVzs7WUFFM0UsTUFBTSxXQUFXLENBQUMsSUFBSSxDQUFDLENBQU8sT0FBTyxFQUFFLEVBQUU7Z0JBQ3JDLE9BQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBZ0IsSUFBSTs7d0JBQ3pDLElBQUksSUFBSSxJQUFJLElBQUksRUFBRTs0QkFDaEIsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7NEJBQzdCLE1BQU0sT0FBTyxDQUFDLEtBQUssRUFBRSxDQUFDO3lCQUN2QjtvQkFDSCxDQUFDO2lCQUFBLENBQUMsQ0FBQTtZQUNOLENBQUMsQ0FBQSxDQUFDLENBQUE7UUFDTixDQUFDO0tBQUE7SUFHWSxtQ0FBbUMsQ0FBQyxXQUE4QixFQUFDLE1BQWEsRUFBQyxLQUFZLEVBQUMsUUFBZSxFQUFDLElBQVc7O1lBRXBJLE1BQU0sV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFPLElBQUksRUFBRSxFQUFFO2dCQUNwQyxNQUFNLElBQUksQ0FBQyxPQUFPLENBQUMsZUFBRSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDLENBQU8sSUFBSSxFQUFFLEVBQUU7b0JBQzNGLElBQUksSUFBSSxJQUFJLElBQUksRUFBRTt3QkFDbEIsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7d0JBQzdCLE1BQU0sSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFTLEVBQUU7d0JBRW5DLENBQUMsQ0FBQSxDQUFDLENBQUE7cUJBQ0o7Z0JBQ0YsQ0FBQyxDQUFBLENBQUMsQ0FBQTtZQUdKLENBQUMsQ0FBQSxDQUFDLENBQUE7UUFDRixDQUFDO0tBQUE7Q0FJSjtBQWpDRCxrREFpQ0MifQ==