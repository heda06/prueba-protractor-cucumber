"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const cucumber_1 = require("cucumber");
const protractor_1 = require("protractor");
const chai = require("chai");
const home_page_1 = require("../page-objets/home-page");
const protractor_function_1 = require("../core/protractor-function");
//import { log4jsconfig } from "../log-config/log4jsconfig"
/** Variable global de verifcacion*/
var { setDefaultTimeout } = require('cucumber');
setDefaultTimeout(60 * 1000);
var expects = chai.expect;
var log4js = require('log4js');
log4js.configure('./log-config/log4js.json');
let home = new home_page_1.HomePage();
let core = new protractor_function_1.ProtractorFunnction();
var log = log4js.getLogger("startup");
cucumber_1.Given('I will navigate to {string}', function (url) {
    return __awaiter(this, void 0, void 0, function* () {
        yield protractor_1.browser.get(url);
        yield protractor_1.browser.waitForAngularEnabled(false);
    });
});
cucumber_1.When('I am searching for the city {string}', function (string) {
    return __awaiter(this, void 0, void 0, function* () {
        yield protractor_1.browser.sleep(2000);
        if (!(yield home.selectorCiudad.isDisplayed())) {
            yield home.selectorCiudad.click();
        }
        core.scrollListOneLevelClick(home.listCity, string);
        // await home.listCity.each(async (element) => {
        //   element.getText().then(async function (text) {
        //     if (text == string) {
        //       await element.click()
        //     }
        //   })
        // })
        yield protractor_1.browser.sleep(2000);
    });
});
cucumber_1.When('I click on  button with the image of {string} {string}', (string, string2) => __awaiter(this, void 0, void 0, function* () {
    // Write code here that turns the phrase above into concrete actions
    if (string == "type") {
        core.scrollListOneLevelClick(home.listType, string2);
    }
    else if (string == "brand") {
        yield home.listBrand.each((item) => __awaiter(this, void 0, void 0, function* () {
            yield item.element(protractor_1.by.className('flex-col-center flwidth flheight cursor item pad16 ng-star-inserted')).$('img.img-res').getAttribute('alt').then((text) => __awaiter(this, void 0, void 0, function* () {
                if (text == string2) {
                    expects(text).to.equal(string2);
                    yield item.click().then(() => __awaiter(this, void 0, void 0, function* () {
                    }));
                }
            }));
        }));
    }
    yield protractor_1.browser.sleep(2000);
}));
cucumber_1.When('I click on the button of the product {string}', (string) => __awaiter(this, void 0, void 0, function* () {
    yield home.listModel.each((item) => __awaiter(this, void 0, void 0, function* () {
        yield item.element(protractor_1.by.className('flex-col-center flwidth flheight cursor item ng-star-inserted')).$('img.img-res').getAttribute('alt').then((text) => __awaiter(this, void 0, void 0, function* () {
            if (text == string) {
                expects(text).to.equal(string);
                yield item.click().then(() => __awaiter(this, void 0, void 0, function* () {
                }));
            }
        }));
        yield protractor_1.browser.sleep(2000);
    }));
}));
cucumber_1.Then('The price is {string}', (string) => __awaiter(this, void 0, void 0, function* () {
    // Write code here that turns the phrase above into concrete actions
    yield home.detailModel.$('meta:nth-child(3)').getAttribute('content').then((text) => __awaiter(this, void 0, void 0, function* () {
        expects(text).to.equal(string);
        console.log("Precio correcto");
    }));
}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RlcHMuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi9zcmMvc3RlcC1kZWZpbmF0aW9uL3N0ZXBzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSx1Q0FBNkM7QUFDN0MsMkNBQXlDO0FBQ3pDLDZCQUE4QjtBQU05Qix3REFBb0Q7QUFDcEQscUVBQWtFO0FBQ2xFLDJEQUEyRDtBQUUzRCxvQ0FBb0M7QUFDcEMsSUFBSSxFQUFFLGlCQUFpQixFQUFFLEdBQUcsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDO0FBQ2hELGlCQUFpQixDQUFDLEVBQUUsR0FBRyxJQUFJLENBQUMsQ0FBQztBQUM3QixJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO0FBQzFCLElBQUksTUFBTSxHQUFHLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztBQUMvQixNQUFNLENBQUMsU0FBUyxDQUFDLDBCQUEwQixDQUFDLENBQUM7QUFDN0MsSUFBSSxJQUFJLEdBQUcsSUFBSSxvQkFBUSxFQUFFLENBQUM7QUFDMUIsSUFBSSxJQUFJLEdBQUcsSUFBSSx5Q0FBbUIsRUFBRSxDQUFDO0FBQ3JDLElBQUksR0FBRyxHQUFHLE1BQU0sQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLENBQUM7QUFHdEMsZ0JBQUssQ0FBQyw2QkFBNkIsRUFBRSxVQUFnQixHQUFHOztRQUN0RCxNQUFNLG9CQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3ZCLE1BQU0sb0JBQU8sQ0FBQyxxQkFBcUIsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUU3QyxDQUFDO0NBQUEsQ0FBQyxDQUFDO0FBSUgsZUFBSSxDQUFDLHNDQUFzQyxFQUFFLFVBQWdCLE1BQU07O1FBRWpFLE1BQU0sb0JBQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7UUFFMUIsSUFBSSxDQUFDLENBQUEsTUFBTSxJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsRUFBRSxDQUFBLEVBQUU7WUFFNUMsTUFBTSxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssRUFBRSxDQUFDO1NBQ25DO1FBQ0QsSUFBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUMsTUFBTSxDQUFDLENBQUM7UUFDbkQsZ0RBQWdEO1FBQ2hELG1EQUFtRDtRQUNuRCw0QkFBNEI7UUFDNUIsOEJBQThCO1FBQzlCLFFBQVE7UUFDUixPQUFPO1FBQ1AsS0FBSztRQUNMLE1BQU0sb0JBQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDNUIsQ0FBQztDQUFBLENBQUMsQ0FBQztBQUdILGVBQUksQ0FBQyx3REFBd0QsRUFBRSxDQUFPLE1BQU0sRUFBRSxPQUFPLEVBQUUsRUFBRTtJQUN2RixvRUFBb0U7SUFFcEUsSUFBSSxNQUFNLElBQUksTUFBTSxFQUFFO1FBRXBCLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFDLE9BQU8sQ0FBQyxDQUFBO0tBQ3BEO1NBQ0ksSUFBSSxNQUFNLElBQUksT0FBTyxFQUMxQjtRQUVFLE1BQU0sSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBTyxJQUFJLEVBQUUsRUFBRTtZQUNyQyxNQUFNLElBQUksQ0FBQyxPQUFPLENBQUMsZUFBRSxDQUFDLFNBQVMsQ0FBQyxxRUFBcUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBTyxJQUFJLEVBQUUsRUFBRTtnQkFDL0osSUFBSSxJQUFJLElBQUksT0FBTyxFQUFFO29CQUNyQixPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztvQkFDaEMsTUFBTSxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQVMsRUFBRTtvQkFFbkMsQ0FBQyxDQUFBLENBQUMsQ0FBQTtpQkFDSjtZQUNGLENBQUMsQ0FBQSxDQUFDLENBQUE7UUFHSixDQUFDLENBQUEsQ0FBQyxDQUFBO0tBRUg7SUFLRCxNQUFNLG9CQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFBO0FBQzNCLENBQUMsQ0FBQSxDQUFDLENBQUM7QUFHSCxlQUFJLENBQUMsK0NBQStDLEVBQUUsQ0FBTyxNQUFNLEVBQUMsRUFBRTtJQUNwRSxNQUFNLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQU8sSUFBSSxFQUFFLEVBQUU7UUFFdkMsTUFBTSxJQUFJLENBQUMsT0FBTyxDQUFDLGVBQUUsQ0FBQyxTQUFTLENBQUMsK0RBQStELENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQU8sSUFBSSxFQUFFLEVBQUU7WUFFekosSUFBSSxJQUFJLElBQUksTUFBTSxFQUFFO2dCQUNwQixPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDL0IsTUFBTSxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQVMsRUFBRTtnQkFFbkMsQ0FBQyxDQUFBLENBQUMsQ0FBQTthQUNKO1FBQ0YsQ0FBQyxDQUFBLENBQUMsQ0FBQTtRQUVGLE1BQU0sb0JBQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUE7SUFFM0IsQ0FBQyxDQUFBLENBQUMsQ0FBQTtBQUVGLENBQUMsQ0FBQSxDQUFDLENBQUM7QUFHSCxlQUFJLENBQUMsdUJBQXVCLEVBQUUsQ0FBTyxNQUFNLEVBQUMsRUFBRTtJQUM1QyxvRUFBb0U7SUFDcEUsTUFBTSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBTyxJQUFJLEVBQUUsRUFBRTtRQUN0RixPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUMvQixPQUFPLENBQUMsR0FBRyxDQUFDLGlCQUFpQixDQUFDLENBQUE7SUFHbEMsQ0FBQyxDQUFBLENBQUMsQ0FBQTtBQUNKLENBQUMsQ0FBQSxDQUFDLENBQUMifQ==