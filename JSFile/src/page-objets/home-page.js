"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const protractor_1 = require("protractor");
/** Clase que contienen los elemntos de la pagina de login de Banca March. */
class HomePage {
    /**
  * Inicializa los elementos para el login
  */
    constructor() {
        this.selectorCiudad = protractor_1.element(protractor_1.by.className('nav-link-label flex-row align-center font-body-2 tc-secondary uppercase font-medium'));
        this.listCity = protractor_1.element.all(protractor_1.by.className("city"));
        this.listType = protractor_1.element.all(protractor_1.by.className('flex-1 flex-row-center flheight product-line ng-star-inserted'));
        this.listBrand = protractor_1.element.all(protractor_1.by.className('grid-item text-center flex-row-center web-view brand-dis ng-star-inserted'));
        this.listModel = protractor_1.element.all(protractor_1.by.className('grid-item text-center flex-row-center web-view product-dis pad8 ng-star-inserted'));
        this.detailModel = protractor_1.element(protractor_1.by.className('pd-detail flex-row flex-col-mob ng-star-inserted'));
    }
}
exports.HomePage = HomePage;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaG9tZS1wYWdlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL3BhZ2Utb2JqZXRzL2hvbWUtcGFnZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLDJDQUF5RTtBQUV6RSw2RUFBNkU7QUFDN0UsTUFBYSxRQUFRO0lBZWhCOztJQUVBO0lBQ0Q7UUFFRyxJQUFJLENBQUMsY0FBYyxHQUFFLG9CQUFPLENBQUMsZUFBRSxDQUFDLFNBQVMsQ0FBQyxxRkFBcUYsQ0FBQyxDQUFDLENBQUM7UUFDbEksSUFBSSxDQUFDLFFBQVEsR0FBRyxvQkFBTyxDQUFDLEdBQUcsQ0FBQyxlQUFFLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7UUFDbEQsSUFBSSxDQUFDLFFBQVEsR0FBRyxvQkFBTyxDQUFDLEdBQUcsQ0FBQyxlQUFFLENBQUMsU0FBUyxDQUFDLCtEQUErRCxDQUFDLENBQUMsQ0FBQztRQUMzRyxJQUFJLENBQUMsU0FBUyxHQUFHLG9CQUFPLENBQUMsR0FBRyxDQUFDLGVBQUUsQ0FBQyxTQUFTLENBQUMsMkVBQTJFLENBQUMsQ0FBQyxDQUFDO1FBQ3hILElBQUksQ0FBQyxTQUFTLEdBQUcsb0JBQU8sQ0FBQyxHQUFHLENBQUMsZUFBRSxDQUFDLFNBQVMsQ0FBQyxrRkFBa0YsQ0FBQyxDQUFDLENBQUM7UUFDL0gsSUFBSSxDQUFDLFdBQVcsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxTQUFTLENBQUMsa0RBQWtELENBQUMsQ0FBQyxDQUFDO0lBQ2hHLENBQUM7Q0FFSjtBQTVCRCw0QkE0QkMifQ==