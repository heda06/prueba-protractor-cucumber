"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const reporter = require("cucumber-html-reporter");
exports.config = {
    directConnect: false,
    framework: 'custom',
    frameworkPath: require.resolve('protractor-cucumber-framework'),
    useBlockingProxy: true,
    webDriverLogDir: './logs',
    capabilities: {
        browserName: 'chrome',
        chromeOptions: {
            args: [
                '--disable-gpu',
                'start-maximized'
                //,'--window-size=800,600'
            ]
        }
    },
    specs: ['../features/busqueda.feature'],
    cucumberOpts: {
        format: 'json:./report/cucumber-report.json',
        //  tags:['@calculatorTesting'],
        require: [
            './src/step-defination/*.js'
        ]
    },
    onComplete: () => {
        var options = {
            theme: 'bootstrap',
            jsonFile: './report/cucumber-report.json',
            output: './report/cucumber-report.html',
            reportSuiteAsScenarios: true,
            launchReport: true,
            metadata: {
                "App Version": "0.3.2",
                "Test Environment": "STAGING",
                "Browser": "Chrome  54.0.2840.98",
                "Platform": "Windows 10",
                "Parallel": "Scenarios",
                "Executed": "Remote"
            }
        };
        reporter.generate(options);
    }
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3VjdW1iZXJDb25maWcuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi9jdWN1bWJlckNvbmZpZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLG1EQUFrRDtBQUN2QyxRQUFBLE1BQU0sR0FBVztJQUV4QixhQUFhLEVBQUMsS0FBSztJQUNuQixTQUFTLEVBQUMsUUFBUTtJQUNsQixhQUFhLEVBQUUsT0FBTyxDQUFDLE9BQU8sQ0FBQywrQkFBK0IsQ0FBQztJQUMvRCxnQkFBZ0IsRUFBRSxJQUFJO0lBQ3RCLGVBQWUsRUFBRSxRQUFRO0lBRXpCLFlBQVksRUFBRTtRQUNWLFdBQVcsRUFBRSxRQUFRO1FBQ3JCLGFBQWEsRUFBRTtZQUNYLElBQUksRUFBRTtnQkFDSixlQUFlO2dCQUNmLGlCQUFpQjtnQkFDakIsMEJBQTBCO2FBQzNCO1NBQ0Y7S0FDTjtJQUVELEtBQUssRUFBRSxDQUFDLDhCQUE4QixDQUFDO0lBQ3ZDLFlBQVksRUFBRTtRQUVWLE1BQU0sRUFBQyxvQ0FBb0M7UUFFN0MsZ0NBQWdDO1FBQzlCLE9BQU8sRUFBRTtZQUNMLDRCQUE0QjtTQUMvQjtLQUNKO0lBQ0QsVUFBVSxFQUFFLEdBQUcsRUFBRTtRQUViLElBQUksT0FBTyxHQUFHO1lBQ2QsS0FBSyxFQUFFLFdBQVc7WUFDbEIsUUFBUSxFQUFFLCtCQUErQjtZQUN6QyxNQUFNLEVBQUUsK0JBQStCO1lBQ3ZDLHNCQUFzQixFQUFFLElBQUk7WUFDNUIsWUFBWSxFQUFFLElBQUk7WUFDbEIsUUFBUSxFQUFFO2dCQUNOLGFBQWEsRUFBQyxPQUFPO2dCQUNyQixrQkFBa0IsRUFBRSxTQUFTO2dCQUM3QixTQUFTLEVBQUUsc0JBQXNCO2dCQUNqQyxVQUFVLEVBQUUsWUFBWTtnQkFDeEIsVUFBVSxFQUFFLFdBQVc7Z0JBQ3ZCLFVBQVUsRUFBRSxRQUFRO2FBQzNCO1NBQ0osQ0FBQztRQUVGLFFBQVEsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7SUFFdkIsQ0FBQztDQUNKLENBQUMifQ==