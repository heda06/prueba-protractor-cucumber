# Prueba de automatizacion con Protractor y Cucumber

_Una pequeña prueba del uso de protractor apoyado en los escenarios creados con cucumber y utlizando el lenguaje typescript_

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._


### Pre-requisitos 📋

_Que cosas necesitas para instalar el software y como instalarlas_

```
Node.js
```

### Instalación 🔧

_Una serie de ejemplos paso a paso que te dice lo que debes ejecutar para tener un entorno de desarrollo ejecutandose_

_Abir el proyecto con Node.js y ejecutar el comando siguiente comando_

```
npm install
```


## Ejecutando las pruebas ⚙️

_Para lanzar el proyecto solo queda ejecutar el archivo de configuracion de cucumber_

```
protractor .\JSFile\cucumberConfig.js
```

_Cada vez que se modifique un archivo TypeScript hay que ejecutar el comando_

```
tsc
```

